<?php

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| Here you may define all of your model factories. Model factories give
| you a convenient way to create models for testing and seeding your
| database. Just tell the factory how a default model should look.
|
*/

use App\Models\Book;
use App\Models\User;
use Illuminate\Database\Eloquent\Factory;
use App\Models\News;

/**
 * @var Factory $factory
 */
$factory->define(User::class, function (Faker\Generator $faker) {
    static $password;

    return [
        'name' => $faker->name,
        'email' => $faker->unique()->safeEmail,
        'password' => $password ?: $password = bcrypt('secret'),
        'remember_token' => str_random(10),
    ];
});

$factory->define(News::class, function (Faker\Generator $faker) {
    return [
        'spoiler' => $faker->sentence(),
        'content' => $faker->text(),
        'published_at' => $faker->dateTime,
    ];
});

$factory->define(Book::class, function (Faker\Generator $faker) {
    return [
        'name' => $faker->name,
        'author' => $faker->userName,
        'description' => $faker->paragraph(),
        'amount' => $faker->randomNumber(),
        'photo' => 'users/default.png',
        'price' => $faker->randomNumber(),
    ];
});
