<?php

namespace App\Http\Controllers;

use App\Http\Requests\Search\SearchRequest;
use App\Models\Book;
use App\Models\News;
use Illuminate\Database\Eloquent\Collection;

class SearchController extends Controller
{
    public function search(SearchRequest $request)
    {
        $query = $request->get('query');
        $results = new Collection();

        $this->searchBooks($query, $results);
        $this->searchNews($query, $results);

        return view('search.results', [
            'query' => $query,
            'results' => $results,
        ]);
    }

    protected function searchBooks($query, Collection $results)
    {
        $books = Book::where('name', 'LIKE', "%$query%")
            ->orWhere('description', 'LIKE', "%$query%")
            ->orWhere('author', 'LIKE', "%$query%")
            ->get();

        foreach ($books as $book) {
            $results->add((object)[
                'url' => route('books.show', $book->id),
                'header' => "Книга: $book->name",
            ]);
        }
    }

    protected function searchNews($query, Collection $results)
    {
        $news = News::where('spoiler', 'LIKE', "%$query%")
            ->orWhere('content', 'LIKE', "%$query%")
            ->get();

        foreach ($news as $newsItem) {
            $results->add((object)[
                'url' => route('news.show', $newsItem->id),
                'header' => "Новость: $newsItem->spoiler",
            ]);
        }
    }
}
