<?php

namespace App\Http\Controllers;

use App\Http\Forms\Feedback\IndexForm;
use App\Http\Requests\Feedback\SearchRequest;
use App\Mail\FeedbackMail;
use App\Models\News;
use Illuminate\Support\Facades\Mail;

class NewsController extends Controller
{
    public function index()
    {
        $news = News::all();

        return view('news.index', [
            'news' => $news,
        ]);
    }

    public function show(News $news)
    {
        return view('news.show', [
            'news' => $news,
        ]);
    }
}
