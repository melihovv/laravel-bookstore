<?php

namespace App\Http\Controllers;

use App\Http\Forms\Books\OrderForm;
use App\Http\Requests\Books\OrderRequest;
use App\Mail\BookOrderMail;
use App\Models\Book;
use Illuminate\Support\Facades\Mail;

class BooksController extends Controller
{
    public function index()
    {
        $books = Book::where('amount', '>', 0)->get();

        return view('books.index', [
            'books' => $books,
        ]);
    }

    public function show(Book $book)
    {
        $form = $this->form(OrderForm::class, [
            'method' => 'POST',
            'url' => route('books.order', $book->id),
        ]);

        return view('books.show', [
            'book' => $book,
            'form' => $form,
        ]);
    }

    public function order(Book $book, OrderRequest $request)
    {
        $mail = new BookOrderMail(
            $book,
            $request->get('email'),
            $request->get('amount')
        );

        Mail::to(config('mail.admin_mail'))
            ->queue($mail);

        return response()
            ->redirectToRoute('books.show', $book->id)
            ->with('success', 'Ваш заказ успешно принят');
    }
}
