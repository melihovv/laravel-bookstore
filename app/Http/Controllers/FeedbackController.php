<?php

namespace App\Http\Controllers;

use App\Http\Forms\Feedback\IndexForm;
use App\Http\Requests\Feedback\SearchRequest;
use App\Mail\FeedbackMail;
use Illuminate\Support\Facades\Mail;

class FeedbackController extends Controller
{
    public function index()
    {
        $form = $this->form(IndexForm::class, [
            'method' => 'POST',
            'url' => route('feedback.send-message'),
        ]);

        return view('feedback.index', [
            'form' => $form,
        ]);
    }

    public function sendMessage(SearchRequest $request)
    {
        $mail = new FeedbackMail(
            $request->get('email'),
            $request->get('message')
        );

        Mail::to(config('mail.admin_mail'))
            ->queue($mail);

        return response()
            ->redirectToRoute('feedback.index')
            ->with('success', 'Ваше сообщение успешно отправлено');
    }
}
