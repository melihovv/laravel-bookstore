<?php

namespace App\Http\Requests\Feedback;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Lang;

class SendMessageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email',
            'message' => 'required|string',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function attributes()
    {
        return [
            'message' => 'сообщение',
        ];
    }

    public function messages()
    {
        return [
            'g-recaptcha-response.*' => Lang::get('auth.captcha_failed')
        ];
    }

}
