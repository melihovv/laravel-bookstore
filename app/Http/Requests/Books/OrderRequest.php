<?php

namespace App\Http\Requests\Books;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Lang;

class OrderRequest extends FormRequest
{
    public function rules()
    {
        return [
            'email' => 'required|email',
            'amount' => 'required|numeric|min:1',
            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function attributes()
    {
        return [
            'amount' => 'количество',
        ];
    }

    public function messages()
    {
        return [
            'g-recaptcha-response.*' => Lang::get('auth.captcha_failed')
        ];
    }

}
