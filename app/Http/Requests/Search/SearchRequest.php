<?php

namespace App\Http\Requests\Search;

use App\Http\Requests\FormRequest;
use Illuminate\Support\Facades\Lang;

class SearchRequest extends FormRequest
{
    public function rules()
    {
        return [
            'query' => 'nullable|string',
        ];
    }
}
