<?php

namespace App\Http\Forms\Feedback;

use Anhskohbo\NoCaptcha\Facades\NoCaptcha;
use Kris\LaravelFormBuilder\Form;

class IndexForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('email', 'email', [
                'label' => 'Адрес электронной почты',
            ])
            ->add('message', 'textarea', [
                'label' => 'Сообщение',
            ])
            ->add('g-recaptcha-response', 'hidden', [
                'showError' => false,
            ])
            ->add('captcha', 'static', [
                'tag' => 'div',
                'label' => false,
                'value' => NoCaptcha::display(),
                'template' => 'laravel-form-builder::captcha',
            ])
            ->add('submit', 'submit', [
                'label' => 'Отправить',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }
}
