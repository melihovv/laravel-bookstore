<?php

namespace App\Http\Forms\Books;

use Anhskohbo\NoCaptcha\Facades\NoCaptcha;
use Kris\LaravelFormBuilder\Form;

class OrderForm extends Form
{
    public function buildForm()
    {
        $this
            ->add('email', 'email', [
                'label' => 'Адрес электронной почты',
            ])
            ->add('amount', 'number', [
                'label' => 'Количество',
                'default_value' => 1,
            ])
            ->add('g-recaptcha-response', 'hidden', [
                'showError' => false,
            ])
            ->add('captcha', 'static', [
                'tag' => 'div',
                'label' => false,
                'value' => NoCaptcha::display(),
                'template' => 'laravel-form-builder::captcha',
            ])
            ->add('submit', 'submit', [
                'label' => 'Заказать',
                'attr' => [
                    'class' => 'btn btn-primary',
                ],
            ]);
    }
}
