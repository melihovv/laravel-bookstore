<?php

namespace App\Mail;

use App\Models\Book;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BookOrderMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var Book
     */
    public $book;

    /**
     * @var string
     */
    public $userEmail;

    /**
     * @var int
     */
    public $amount;

    /**
     * @param Book $book
     * @param string $email
     * @param int $amount
     */
    public function __construct(Book $book, $email, $amount)
    {
        $this->userEmail = $email;
        $this->amount = $amount;
        $this->book = $book;
    }

    public function build()
    {
        return $this
            ->markdown('emails.book-order', [
                'userEmail' => $this->userEmail,
                'amount' => $this->amount,
                'book' => $this->book,
            ])
            ->from($this->userEmail)
            ->subject('Сообщение с формы заказ книги');
    }
}
