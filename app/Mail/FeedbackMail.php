<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class FeedbackMail extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * @var string
     */
    public $userEmail;

    /**
     * @var string
     */
    public $message;

    /**
     * @param string $email
     * @param string $message
     */
    public function __construct($email, $message)
    {
        $this->userEmail = $email;
        $this->message = $message;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this
            ->markdown('emails.feedback', [
                'from' => $this->userEmail,
                'message' => $this->message,
            ])
            ->from($this->userEmail)
            ->subject('Сообщение с формы обратной связи');
    }
}
