<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'spoiler',
        'content',
        'published_at',
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'published_at',
    ];

    public function getPresentPublishedAtAttribute()
    {
        return $this->published_at->toFormattedDateString();
    }
}
