<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    use AdditionalMethods;

    protected $fillable = [
        'name',
        'author',
        'description',
        'amount',
        'photo',
        'price',
    ];

    protected $casts = [
        'amount' => 'integer',
        'price' => 'integer',
    ];

    public function getPresentPhotoAttribute()
    {
        return "/storage/$this->photo";
    }
}
