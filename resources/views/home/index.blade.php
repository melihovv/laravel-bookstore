@extends('layouts.master')

@section('content')
    <div class="jumbotron">
        <h1>Магазин книг</h1>
        <p>Здесь вы можете найти и заказать интересущие вас книги</p>
        <p><a class="btn btn-primary btn-lg" href="{{ route('books.index') }}" role="button">В каталог книг</a></p>
    </div>
@endsection
