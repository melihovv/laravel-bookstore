@extends('layouts.master')

@section('title', 'Книги')

@section('content')
    <div class="page-header">
        <h1>Книги</h1>
    </div>

    @if ($books->count())
        <div class="books-page">
            @foreach ($books->chunk(2) as $chunk)
                <div class="row">
                    @foreach ($chunk as $book)
                        <div class="col-sm-6 books-page__book">
                            <a href="{{ route('books.show', $book->id) }}">
                                <div class="media">
                                    <div class="media-left">
                                        <img class="media-object" src="{{ $book->presentPhoto }}">
                                    </div>
                                    <div class="media-body">
                                        <h2 class="media-heading">{{ $book->name }}</h2>
                                        <p>Автор: {{ $book->author }}</p>
                                        <p>Цена: {{ $book->price }} руб.</p>
                                    </div>
                                </div>
                            </a>
                        </div>
                    @endforeach
                </div>
            @endforeach
        </div>
    @else
        <p>На данный момент в нашем магазине нет книг</p>
    @endif
@endsection
