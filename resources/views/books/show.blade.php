@extends('layouts.master')

@section('title', $book->name)

@section('content')
    <div class="row">
        <div class="col-sm-8">
            <div class="book">
                <img src="{{ $book->presentPhoto }}">
                <h2>{{ $book->name }}</h2>
                <p>Автор: {{ $book->author }}</p>
                <p>Цена: {{ $book->price }} руб.</p>
                <p>{{ $book->description }}</p>
            </div>
        </div>
        <div class="col-sm-4">
            {!! form($form) !!}
        </div>
    </div>
@endsection
