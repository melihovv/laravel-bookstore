<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@hasSection('title')@yield('title') - {{ config('app.name') }}@else{{ config('app.name') }}@endif</title>

    <link rel="stylesheet" href="{{ mix('/css/app.css') }}">

    <script>
        window.Laravel = @php echo json_encode([
            'csrfToken' => csrf_token(),
        ]); @endphp
    </script>
</head>
<body>
<div id="app">
    <div class="container">
        @include('layouts._navbar')
        @include('layouts._messages')

        @yield('content')
    </div>
</div>

<script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>
