<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed"
                    data-toggle="collapse"
                    data-target="#app-navbar-collapse" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">Магазин книг</a>
        </div>

        <div class="collapse navbar-collapse" id="app-navbar-collapse">
            <ul class="nav navbar-nav">
                <li><a href="/">Главная</a></li>
                <li><a href="{{ route('books.index') }}">Книги</a></li>
                <li><a href="{{ route('news.index') }}">Новости</a></li>
                <li><a href="{{ route('feedback.index') }}">Обратная связь</a></li>
            </ul>

            <form class="navbar-form navbar-right"
                  action="{{ route('search.search') }}">
                <div class="form-group">
                    <input type="text" class="form-control"
                           placeholder="Поиск по сайту" name="query">
                </div>

                <button type="submit" class="btn btn-default">Искать</button>
            </form>
        </div>
    </div>
</nav>
