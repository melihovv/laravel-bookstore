@component('mail::message')

@component('mail::panel')
    Пользователь с адресом электронной почты {{ $userEmail }} заказал книгу
    с названием "{{ $book->name }}" в количестве {{ $amount }}
@endcomponent

@endcomponent
