@component('mail::message')

Пользователь с адресом электронной почты {{ $from }} оставил следующее сообщение

@component('mail::panel')
    {{ $message }}
@endcomponent

@endcomponent
