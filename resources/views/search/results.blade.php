@extends('layouts.master')

@section('title', 'Результаты поиска')

@section('content')
    <div class="page-header">
        <h1>Результаты поиска по запросу "{{ $query }}"</h1>
    </div>

    @php /** @var \Illuminate\Support\Collection $results */ @endphp
    @if ($results->count() > 0)
        @foreach ($results as $result)
            <div class="panel panel-default">
                <div class="panel-body">
                    <h2>
                        <a href="{{ $result->url }}">{{ $result->header }}</a>
                    </h2>
                </div>
            </div>
        @endforeach
    @else
        <p>Ничего не нашлось</p>
    @endif
@endsection
