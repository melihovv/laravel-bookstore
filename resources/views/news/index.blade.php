@extends('layouts.master')

@section('title', 'Новости')

@section('content')
    <div class="page-header">
        <h1>Новости</h1>
    </div>

    @forelse ($news as $newsItem)
        <div class="panel panel-default">
            <div class="panel-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-10">
                            <h2>
                                <a href="{{ route('news.show', $newsItem->id) }}">
                                    {{ $newsItem->spoiler }}
                                </a>
                            </h2>
                        </div>
                        <div class="col-sm-2">
                            <p>
                                {{ $newsItem->presentPublishedAt }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @empty
        <p>Пока что новостей нет</p>
    @endforelse
@endsection
