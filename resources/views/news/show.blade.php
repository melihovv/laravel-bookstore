@extends('layouts.master')

@section('title', $news->spoiler)

@section('content')
    <h1 class="well">{{ $news->spoiler }}</h1>
    <p>{{ $news->presentPublishedAt }}</p>
    <p>{{ $news->content }}</p>
@endsection
