@extends('layouts.master')

@section('title', 'Обратная связь')

@section('content')
    <h1>Обратная связь</h1>
    {!! form($form) !!}
@endsection
