@php
    $showError = isset($options['showError'])
        ? $options['showError']
        : $showError;
@endphp

@if ($showLabel && $showField)
    @if ($options['wrapper'] !== false)
        <div {!! $options['wrapperAttrs'] !!}>
    @endif
@endif

@if ($showLabel && $options['label'] !== false && $options['label_show'])
    {!! Form::customLabel($name, $options['label'], $options['label_attr']) !!}
@endif

@if ($showField)
    {!! Form::input($type, $name, $options['value'], $options['attr']) !!}

    @include('laravel-form-builder::help_block')
    @include('laravel-form-builder::errors')
@endif

@if ($showLabel && $showField)
    @if ($options['wrapper'] !== false)
        </div>
    @endif
@endif
