@extends('laravel-form-builder::static')

@section('errors')
    @if (isset($errors))
        @foreach ($errors->get('g-recaptcha-response') as $error)
            <div <?= $options['errorAttrs'] ?>><?= $error ?></div>
        @endforeach
    @endif
@overwrite

