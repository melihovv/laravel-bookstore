<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/logs', [
    'uses' => '\Melihovv\LaravelLogViewer\LaravelLogViewerController@index',
    'as' => 'logs',
]);

Route::get('/', 'HomeController@index')
    ->name('home.index');

Route::get('/feedback', 'FeedbackController@index')
    ->name('feedback.index');

Route::post('/feedback', 'FeedbackController@sendMessage')
    ->name('feedback.send-message');

Route::get('/news', 'NewsController@index')
    ->name('news.index');

Route::get('/news/{news}', 'NewsController@show')
    ->name('news.show');

Route::get('/books', 'BooksController@index')
    ->name('books.index');

Route::get('/books/{book}', 'BooksController@show')
    ->name('books.show');

Route::post('/books/{book}/order', 'BooksController@order')
    ->name('books.order');

Route::get('/search', 'SearchController@search')
    ->name('search.search');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
